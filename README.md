# k8s-test-app

A simple test application for kubernetes. The application is written in nodeJS and
can be used to demonstrate how a node application can be containerized and run in a
kubernetes cluster

## Quick start kubernetes
Prereq helm installed in kubernetes cluster.

`helm install --name <name> test-app-chart`

## Server
the server is run by using the command `node server.js`. The server will start and connect to a
mongoDB database. The location of the database is set with environment variable DB_DNS.
The dockerfile can be located at dockerhub.com/vanneback/test-app-backend

## Client
The client can be quick started by using `npm start`. In production it is deployed with 
`npm run build` and then it is run with as a nginx server. The production is built to
run in a kubernetes cluster and the backend dns is set by setting the environment variables
`BACKEND_SERVICE` and `BACKEND_NAMESPACE`. The server is then run by running the script 
in client/template.
The dockerfile can be located at dockerhub.com/vanneback/test-app-frontend

## Database
The database uses mongoDB at port 27017. The database can be started locally in a dockercontainer
with `docker run --rm -p 27017:27017 mongo`


----------------------------

## CICD
These are some steps to setup CICD without using runners in the cluster

1. Create a service account for gitlab in the cluster and give this SA desired permissions
2. Go to settings--\>cicd--\>environment variables.
3. Set the variables:
  * KUBERNETES\_CA
  * KUBERNETES\_TOKEN
  * KUBERNETES\_URL 
4. The variables can be found easily by running the script with: `sh get-auth.sh <name of SA>`


Now the variables set can be accessed from the image `vanneback/kubectl-helm`.
To set the variables in the dockerfile the script `/opt/initK8s.sh` should be run first.
See [Dockerhub](https://cloud.docker.com/u/vanneback/repository/docker/vanneback/kubectl-helm)
and [Gitlab](https://gitlab.com/vanneback/kubectl-helm-dockerfile) for info of how the 
image is built.

## .gitlab-ci.yml
I have started the pipeline but have not finished. (The pipeline in gitlab is not responding atm..)
Anyway what needs to be done is:

* Push the helm-chart to a helm repository and decide on how to
version it.  
* Decide how to tag the dockerfile and helmcharts.
* Should brances or tags be used to decide when a release is done for deployment to prod.
* Sample-application gets error when launched (the mongodb gets stuck in crashloop).
* Add secrets to jfrog and private repos.

Realised the stuff i have in the pipeline now only takes regard of one docker image from the
initial repo. This wont work because we have three different dockerfiles. This has to be fixed

Sorry for the rushed documentation and unfinished steps but had a lot of issues today
and ran out of time..
